Getimap
=======

GTMP (General Time Manipulation Program) has many useful tools to do with time. It has no time travel (which is what people think of with manipulation).
It requires python 2.x.x (preferably 2.7.x (you can get 2.7.5 here: http://www.python.org/download/releases/2.7.5/)). You need windows for this. You no longer need to install a module, but it is preferable. Get it at http://sourceforge.net/projects/pywin32/files/pywin32/Build%20218/. If enough
people want it for other systems, I will. It may not offer the same functionality though.
